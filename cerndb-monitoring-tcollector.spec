Summary	:	This package contains the tcollector tools to send metrics to OpenTSDB.
Name:		cerndb-monitoring-tcollector
Version:	0.0.1
Release:	1%{?dist}
Requires:       python
BuildRequires:	git
License:	GPL
BuildArch:	noarch
Group:		Development/Tools
Source:		%{name}-%{version}.tar.gz
BuildRoot:	%{_builddir}/%{name}-root
AutoReqProv:	no


%description
This package contains the tcollector tools to send metrics to OpenTSDB.

%prep
echo "This is prep"
%setup

%build
echo "This is build"

%install
echo "This is install"

mkdir -p $RPM_BUILD_ROOT/usr/local/tcollector
cp -a ./tcollector/* $RPM_BUILD_ROOT/usr/local/tcollector/

%clean
echo "This is clean"

%files
%defattr(-,root,root,-)
/usr/local/tcollector/*

%changelog
* Wed Jul 22 2015 Charles Newey <charles.newey@cern.ch> 0.1-1
- Initial creation of tcollector package.
